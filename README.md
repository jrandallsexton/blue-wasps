<h3>So Peter is not remembering to include the coversheet on his TPS reports.  He "says" that he saw the memo, but from now on, every time he submits a report, all of his bosses and co-workers are going to remind him.

Other things are set up in this such as the trx-svc which would begin the "rounding issue" that Michael exploited.

30,000 foot overview:</h3>

```mermaid
graph LR
    subgraph gateway
        B[bw.gateway]
    end
    A[bw.mvc.ui] -->|/reports| B
    subgraph report-svc
        B --> C[bw.report-svc]
        C --> id1[(report-svc-db)]
    end
    id1 --> C
    C -->|reportEvents| D[alert-svc]
    A -->|/actions| B
    subgraph contact-svc
        B --> E[bw.contact-svc]
        E --> cdb[(contact-svc-db)]    
    end
    C --> E
    cdb --> E    
    subgraph alert-svc
        E -->|contactEvents| D[alert-svc]
        F -->|trxEvents| D[alert-svc]
        D --> adb[(alert-svc-db)]
    end
    subgraph trx-svc
        F[bw.trx-svc]
        G[bw.trx-fx] -->F
        F --> tdb[(trx-db)]
    end
    tdb --> F
    D --> H[signalR]
    H --> A 
```
Without making the diagram too convoluted, this portion has been omitted and placed here:

```mermaid
graph LR
    A[event] --> B(az-svc-bus-topic)
    B --> C(az-queues)
    C --> D(event-subscribers)
```
